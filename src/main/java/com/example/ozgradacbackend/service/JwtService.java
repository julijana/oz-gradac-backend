package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.User;

public interface JwtService {

  /**
   * Checks if a given token exists in database
   *
   * @param jwtToken as {@link String}
   * @return true if token exists in database
   */
  boolean tokenExists(String jwtToken);

  /**
   * Creates {@link com.example.ozgradacbackend.entity.Jwt
   * @param jwtToken
   * @param user
   */
  void createJwt(String jwtToken, User user);
}
