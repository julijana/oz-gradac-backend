package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.File;
import com.example.ozgradacbackend.repository.FileRepository;
import com.example.ozgradacbackend.service.FileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@AllArgsConstructor
public class FileServiceImpl implements FileService {

  private final FileRepository fileRepository;

  public File save(MultipartFile multipartFile) throws IOException {

    String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

    File file = new File(fileName, multipartFile.getContentType(), multipartFile.getBytes());
    return fileRepository.save(file);
  }
  @Override
  public File getFile(Long id) {
    return fileRepository.findById(id).get();
  }

}
