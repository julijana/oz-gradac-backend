package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.Jwt;
import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.repository.JwtRepository;
import com.example.ozgradacbackend.service.JwtService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class JwtServiceImpl implements JwtService {

  private final JwtRepository jwtRepository;

  @Override
  public boolean tokenExists(String jwtToken) {
    return jwtRepository.findByToken(jwtToken).isPresent();
  }

  @Override
  public void createJwt(String jwtToken, User user) {
    Jwt jwt = Jwt.builder()
        .token(jwtToken)
        .user(user)
        .build();
    jwtRepository.save(jwt);
  }
}
