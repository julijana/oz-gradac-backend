package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.Category;
import com.example.ozgradacbackend.entity.File;
import com.example.ozgradacbackend.entity.dto.CategoryDto;
import com.example.ozgradacbackend.entity.dto.CategoryResponseDto;
import com.example.ozgradacbackend.repository.CategoryRepository;
import com.example.ozgradacbackend.service.CategoryService;
import com.example.ozgradacbackend.service.FileService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;
  private final FileService fileService;
  private final ModelMapper modelMapper;

  @Override
  public CategoryDto createCategory(CategoryDto categoryDto) {
    Category category = modelMapper.map(categoryDto, Category.class);
    Category savedCategory = categoryRepository.save(category);
    return modelMapper.map(savedCategory, CategoryDto.class);
  }

  @Override
  public List<Category> getAllCategories() {
    return categoryRepository.findAll();
  }

  @Override
  public void addFile(MultipartFile multipartFile, Long categoryId)
      throws IOException {
    Optional<Category> category =
        Optional.ofNullable(categoryRepository.getReferenceById(categoryId));
    if (!category.isPresent()) {
      throw new RuntimeException("Category with this id doesn't exist");
    }

    File file = fileService.save(multipartFile);
    category.get().setPicture(file);
    categoryRepository.save(category.get());
  }
}
