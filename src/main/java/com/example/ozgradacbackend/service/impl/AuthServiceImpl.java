package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.entity.dto.UserLoginRequestDto;
import com.example.ozgradacbackend.entity.dto.UserLoginResponseDto;
import com.example.ozgradacbackend.entity.dto.UserRegisterRequestDto;
import com.example.ozgradacbackend.entity.dto.UserRegisterResponseDto;
import com.example.ozgradacbackend.exception.InvalidLoginException;
import com.example.ozgradacbackend.exception.InvalidUserRegistrationException;
import com.example.ozgradacbackend.service.AuthService;
import com.example.ozgradacbackend.service.JwtService;
import com.example.ozgradacbackend.service.UserService;
import com.example.ozgradacbackend.util.JwtUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
@AllArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

  private static final String cookieName = "JWT";
  private final UserService userService;
  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;

  private final ModelMapper modelMapper;

  @Override
  public UserRegisterResponseDto registerUser(UserRegisterRequestDto userRegisterRequestDto) {

    try {
      UserRegisterResponseDto userRegisterResponseDto = new UserRegisterResponseDto();
      if (userService.isUserRegistered(userRegisterRequestDto.getUsername()))
        throw new InvalidUserRegistrationException("User already exists!");

      userService.createUser(userRegisterRequestDto);
      userRegisterResponseDto.setMessage("User registered successfully!");

      return userRegisterResponseDto;
    }catch (Exception exception){
      throw new InvalidUserRegistrationException("Registration failed!");
    }
  }

  @Override
  public UserLoginResponseDto signIn(UserLoginRequestDto userLoginRequestDto, HttpServletResponse response) {

    UserLoginResponseDto userLoginResponseDto = new UserLoginResponseDto();
    try{
      Authentication authentication = authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(userLoginRequestDto.getUsername(), userLoginRequestDto.getPassword())
      );
      SecurityContextHolder.getContext().setAuthentication(authentication);
      String jwt = JwtUtils.generateJwtToken(authentication);
      User currentUser = userService.getCurrentUser(authentication);
      jwtService.createJwt(jwt, currentUser);
      addJwtAsCookie(response, jwt);
      userLoginResponseDto.setMessage("User logged in successfully!");
      userLoginResponseDto.setJwt(jwt);
      userLoginResponseDto.setUser(currentUser);

      return  userLoginResponseDto;

    }catch (Exception exception){
      throw new InvalidLoginException("Login failed!");
    }
  }

  private void addJwtAsCookie(HttpServletResponse response, String jwt){
    Cookie cookie = new Cookie(cookieName, jwt);
    cookie.setHttpOnly(true);
    cookie.setPath("/");

    try{
      URI uri = URI.create("http://localhost:8080");
      cookie.setSecure(uri.getScheme().equals("https"));
      cookie.setDomain(uri.getHost());
    }catch (IllegalArgumentException exception){
      log.error("Could not set domain on Cookie to {}", exception.getMessage());
    }
    response.addCookie(cookie);
  }
}
