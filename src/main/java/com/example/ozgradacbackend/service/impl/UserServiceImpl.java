package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.entity.dto.UserRegisterRequestDto;
import com.example.ozgradacbackend.enums.Role;
import com.example.ozgradacbackend.exception.UserNotFoundException;
import com.example.ozgradacbackend.repository.UserRepository;
import com.example.ozgradacbackend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @Override
  public void createUser(UserRegisterRequestDto userRegisterRequestDto) {

    User user = User.builder()
        .username(userRegisterRequestDto.getUsername())
        .email(userRegisterRequestDto.getEmail())
        .firstName(userRegisterRequestDto.getFirstName())
        .lastName(userRegisterRequestDto.getLastName())
        .phone(userRegisterRequestDto.getPhone())
        .role(Role.ADMIN)
        .password(passwordEncoder.encode(userRegisterRequestDto.getPassword()))
        .build();

    userRepository.save(user);
  }

  @Override
  public User getUserByUsername(String username) throws UserNotFoundException {
    return userRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException("User not found " + username));
  }

  @Override
  public User getCurrentUser(Authentication authentication) {
    User principal = (User) authentication.getPrincipal();
    return getUserByUsername(principal.getUsername());
  }

  @Override
  public List<User> getAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public boolean isUserRegistered(String username){
    return userRepository.findByUsername(username).isPresent();
  }
}
