package com.example.ozgradacbackend.service.impl;

import com.example.ozgradacbackend.entity.Post;
import com.example.ozgradacbackend.entity.dto.PostDto;
import com.example.ozgradacbackend.repository.PostRepository;
import com.example.ozgradacbackend.service.PostService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

  private final PostRepository postRepository;
  private final ModelMapper modelMapper;
  @Override
  public Post createPost(PostDto postDto) {
    Post post = modelMapper.map(postDto, Post.class);
    return postRepository.save(post);
  }

  @Override
  public List<Post> getAllPosts() {
    return postRepository.findAll();
  }
}
