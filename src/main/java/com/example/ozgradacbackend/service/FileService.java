package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.File;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {

  public File save(MultipartFile file) throws IOException;

  public File getFile(Long id);
}
