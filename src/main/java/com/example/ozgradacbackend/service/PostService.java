package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.Post;
import com.example.ozgradacbackend.entity.dto.PostDto;

import java.util.List;

public interface PostService {

  public Post createPost(PostDto postDto);

  public List<Post> getAllPosts();
}
