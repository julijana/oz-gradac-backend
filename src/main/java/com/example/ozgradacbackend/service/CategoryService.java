package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.Category;
import com.example.ozgradacbackend.entity.File;
import com.example.ozgradacbackend.entity.dto.CategoryDto;
import com.example.ozgradacbackend.entity.dto.CategoryResponseDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CategoryService {
  CategoryDto createCategory(CategoryDto categoryDto);
  List<Category> getAllCategories();
  void addFile(MultipartFile multipartFile, Long categoryId) throws IOException;
}
