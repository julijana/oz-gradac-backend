package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.dto.UserLoginRequestDto;
import com.example.ozgradacbackend.entity.dto.UserLoginResponseDto;
import com.example.ozgradacbackend.entity.dto.UserRegisterRequestDto;
import com.example.ozgradacbackend.entity.dto.UserRegisterResponseDto;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Wraps all JWT authentication relevant service calls
 */
public interface AuthService {

  /**
   * Register a new user by providing a {@link UserRegisterRequestDto} object
   * that contains the username, password and email address
   * @param userRegisterRequestDto is a {@link UserRegisterRequestDto}
   * @return a {@link UserRegisterResponseDto} with a message about the registration
   */
  UserRegisterResponseDto registerUser(UserRegisterRequestDto userRegisterRequestDto);

  /**
   * Sign in a registered user.
   * @param userLoginRequestDto is a {@link UserLoginRequestDto} object which contains
   * username and password of existing user.
   * @param response {@link HttpServletResponse} used to add a JWT as a cookie if a user
   * is authenticated
   * @return {@link UserLoginRequestDto} with either a JWT or a message that the authentication failed.
   */
  UserLoginResponseDto signIn(UserLoginRequestDto userLoginRequestDto, HttpServletResponse response);

}

