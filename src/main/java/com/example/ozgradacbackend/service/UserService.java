package com.example.ozgradacbackend.service;

import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.entity.dto.UserRegisterRequestDto;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface UserService {

  boolean isUserRegistered(String username);

  void createUser(UserRegisterRequestDto userRegisterRequestDto);

  User getUserByUsername(String username);

  User getCurrentUser(Authentication authentication);

  List<User> getAllUsers();
}
