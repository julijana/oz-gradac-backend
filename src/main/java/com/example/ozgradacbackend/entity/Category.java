package com.example.ozgradacbackend.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "category")
public class Category {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "created")
  private LocalDateTime created = LocalDateTime.now();

  @Column(name = "category")
  private String category;

  @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
  private List<Post> posts;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "picture", referencedColumnName = "id")
  private File picture;

}
