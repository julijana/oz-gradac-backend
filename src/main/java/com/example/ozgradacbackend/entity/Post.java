package com.example.ozgradacbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "post")
public class Post {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "post_id")
  private Long postId;

  @Column(name = "title")
  private String title;

  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "category_id", referencedColumnName = "id")
  @JsonIgnore
  private Category category;


  @Column(name = "location")
  private String location;

  @Column(name = "price")
  private String price;

  @Column(name = "status")
  private String status;

  @Column(name = "workTime")
  private String workTime;

  @Column(name = "gender")
  private String gender;

  @Column(name = "created")
  private LocalDateTime created = LocalDateTime.now();

  @Column(name = "tags")
  private List<String> tags = new ArrayList<>();

  @Column(name = "description")
  private String description;
}
