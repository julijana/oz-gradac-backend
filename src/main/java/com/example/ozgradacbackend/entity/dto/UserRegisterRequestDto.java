package com.example.ozgradacbackend.entity.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegisterRequestDto {

  @NotEmpty(message = "Username can not be null or empty")
  private String username;

  @NotEmpty(message = "Email can not be null or empty")
  @Email
  private String email;

  @NotEmpty(message = "Password can not be null or empty")
  private String password;

  @NotEmpty(message = "First name can not be null or empty")
  private String firstName;

  @NotEmpty(message = "Last name can not be null or empty")
  private String lastName;

  @NotEmpty(message = "Phone can not be null or empty")
  private String phone;

}
