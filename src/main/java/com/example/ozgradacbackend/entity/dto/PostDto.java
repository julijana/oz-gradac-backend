package com.example.ozgradacbackend.entity.dto;


import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PostDto {
  @NotEmpty(message = "Post id can not be null or empty")
  private Long postId;
  @NotEmpty(message = "Title can not be null or empty")
  private String title;
  @NotEmpty(message = "Category can not be null or empty")
  private CategoryDto category;
  @NotEmpty(message = "Location can not be null or empty")
  private String location;

  @NotEmpty(message = "Price can not be null or empty")
  private String price;
  @NotEmpty(message = "Status can not be null or empty")
  private String status;
  @NotEmpty(message = "Work time can not be null or empty")
  private String workTime;
  @NotEmpty(message = "Created dated can not be null or empty")
  private LocalDateTime created;
  @NotEmpty(message = "Gender can not be null or empty")
  private String gender;
  private List<String> tags;
  @NotEmpty(message = "Description can not be null or empty")
  private String description;
}
