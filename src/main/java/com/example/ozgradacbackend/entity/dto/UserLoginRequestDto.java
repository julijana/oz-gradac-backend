package com.example.ozgradacbackend.entity.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class UserLoginRequestDto {

  @NotEmpty(message = "Username can not be null or empty")
  private String username;

  @NotEmpty(message = "Password can not be null or empty")
  private String password;
}
