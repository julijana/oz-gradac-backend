package com.example.ozgradacbackend.entity.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CategoryDto {

  private Long id;

  @NotEmpty(message = "Category can not be null or empty")
  private String category;

  @NotEmpty(message = "Date can not be null or empty")
  private LocalDateTime created;

}
