package com.example.ozgradacbackend.entity.dto;

import com.example.ozgradacbackend.entity.User;
import lombok.Data;

/** Response after logging in a user with jwt */
@Data
public class UserLoginResponseDto {

  /** a message provided by login */
  private String message;

  /** the jwt to authenticate the requests */
  private String jwt;

  /** loggedIn user */
  private User user;


}
