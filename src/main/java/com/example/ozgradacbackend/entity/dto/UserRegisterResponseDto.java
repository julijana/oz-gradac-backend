package com.example.ozgradacbackend.entity.dto;

import lombok.Data;

/** Response after register a user */
@Data
public class UserRegisterResponseDto {

  /** a message provided by register */
  private String message;
}
