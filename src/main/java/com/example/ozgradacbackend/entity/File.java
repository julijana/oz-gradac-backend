package com.example.ozgradacbackend.entity;

import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "file")
public class File {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "type")
  private String type;

  @Lob
  @Column(name = "image_url")
  private byte[] imageUrl;

  @OneToOne(mappedBy = "picture")
  private Category category;

  public File(String name, String type, byte[] imageUrl) {
    this.name = name;
    this.type = type;
    this.imageUrl = imageUrl;
  }
}
