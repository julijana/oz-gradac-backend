package com.example.ozgradacbackend.filter;

import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.exception.JwtTokenNotFoundException;
import com.example.ozgradacbackend.exception.UserNotFoundException;
import com.example.ozgradacbackend.service.JwtService;
import com.example.ozgradacbackend.service.UserService;
import com.example.ozgradacbackend.util.JwtUtils;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Slf4j
@Component
@AllArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {

  private final JwtService jwtService;
  private final UserService userService;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain)
      throws ServletException, IOException {
     log.debug("JwtRequestFilter has been called; URI: {}", request.getRequestURI());

    String jwt = JwtUtils.extractJwt(request);

    try{
      if(jwt == null){
        throw new IllegalArgumentException("Jwt should not be null!");
      }
      JwtUtils.validateToken(jwt);
      if(!jwtService.tokenExists(jwt)){
        throw new JwtTokenNotFoundException("Token has to exist in database.");
      }
      String username = JwtUtils.extractUsernameFromJwt(jwt);
      User user = userService.getUserByUsername(username);
      UsernamePasswordAuthenticationToken authenticationToken =
          new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
      authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    } catch (JwtException | IllegalArgumentException exception){
      // if jwt is null or jwt validation failed
      log.trace("Jwt is invalid: {}", exception.getMessage());
      SecurityContextHolder.getContext().setAuthentication(null);
    }
    catch(UserNotFoundException exception){
      // userService.getUserByUsername
      log.trace("User not found: {}", exception.getMessage());
      SecurityContextHolder.getContext().setAuthentication(null);
    }finally {
      filterChain.doFilter(request, response);
    }
  }
}
