package com.example.ozgradacbackend.util;

import com.example.ozgradacbackend.entity.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;

/** JwtUtils to encapsulate the JWT handling */
@Slf4j
public class JwtUtils {

  private static final String SECRET_KEY = "6A576E5A7234753777217A25432A462D4A614E645267556B5870327335763879";

  private static final Long EXPIRATION = Long.valueOf(86400000);
  /**
   * Private default constructor to prevent Java to add an implicit public constructor because
   * this class should not be instantiated.
   */
  private JwtUtils(){

  }

  /**
   * The method to generate a jwt
   * @param authentication the {@link Authentication} used to generate the jwt
   * @return Jwt as a {@link String}
   */
  public static String generateJwtToken(Authentication authentication){

    User user = (User) authentication.getPrincipal();
    return Jwts
        .builder()
        .setClaims(new HashMap<>())
        .setSubject(user.getUsername())
        .setIssuedAt(new Date())
        .setExpiration(new Date(new Date().getTime() + EXPIRATION))
        .signWith(getSignInKey())
        .compact();
  }

  private static Key getSignInKey(){
    byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
    return Keys.hmacShaKeyFor(keyBytes);
  }


  /**
   * Extract the JWT from given {@link HttpServletRequest}
   * @param request {@link HttpServletRequest} The JWT will be extracted from the
   * "Authorization"- Header removing the "Bearer" substring
   * @return the JWT as a {@link String}
   */
  public static String extractJwt(HttpServletRequest request) throws ServletException {
    String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
    String jwt = null;
    if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
      jwt = authorizationHeader.substring(7);
    }
    return  jwt;
  }

  /**
   * Extract the username from given jwt
   * @param jwt {@link String} the jwt to extract the username from
   * @return the username as {@link String}
   */
  public static String extractUsernameFromJwt(String jwt){
    return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(jwt).getBody().getSubject();
  }

  /**
   * Validates the given jwt by parsing the jwt and the claims
   * @param jwt {@link String} the jwt to be parsed
   * @throws io.jsonwebtoken.JwtException if the jwt cannot be parsed
   */
  public static void validateToken(String jwt){
    Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(jwt);
  }
}
