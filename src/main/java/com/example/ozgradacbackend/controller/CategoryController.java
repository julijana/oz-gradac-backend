package com.example.ozgradacbackend.controller;

import com.example.ozgradacbackend.entity.Category;
import com.example.ozgradacbackend.entity.dto.CategoryDto;
import com.example.ozgradacbackend.entity.dto.CategoryResponseDto;
import com.example.ozgradacbackend.service.CategoryService;
import com.example.ozgradacbackend.service.FileService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/category", produces = {MediaType.APPLICATION_JSON_VALUE})
@AllArgsConstructor
public class CategoryController {

  private final CategoryService categoryService;
  private final ModelMapper modelMapper;
  private final FileService fileService;

  @GetMapping("/")
  public ResponseEntity<List<CategoryDto>> getAllCategories() {

    List<Category> returnedCategories = categoryService.getAllCategories();
    List<CategoryDto> categoryDtos =
        returnedCategories.stream().map(category -> modelMapper.map(category, CategoryDto.class))
            .collect(Collectors.toList());
    return ResponseEntity.ok(categoryDtos);
  }

  @PostMapping("/")
  public ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CategoryDto categoryDto) {

    CategoryDto categoryResponseDto = categoryService.createCategory(categoryDto);

    return ResponseEntity.ok(categoryResponseDto);
  }

  public ResponseEntity<CategoryResponseDto> addFile(
      @RequestParam("file") MultipartFile multipartFile,
      @RequestParam("categoryId") Long categoryId) {

    CategoryResponseDto categoryResponseDto = new CategoryResponseDto();
    try {
      categoryService.addFile(multipartFile, categoryId);
      categoryResponseDto.setMessage("Kategorija uspesno kreirana");
    } catch (IOException e) {
      categoryResponseDto.setMessage("Greska prilikom upload-ovanja fotografije.");
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(categoryResponseDto);
    }
    return ResponseEntity.ok(categoryResponseDto);
  }
}
