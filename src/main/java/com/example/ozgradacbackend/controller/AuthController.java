package com.example.ozgradacbackend.controller;

import com.example.ozgradacbackend.entity.dto.UserLoginRequestDto;
import com.example.ozgradacbackend.entity.dto.UserLoginResponseDto;
import com.example.ozgradacbackend.entity.dto.UserRegisterRequestDto;
import com.example.ozgradacbackend.service.AuthService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller provides access to the API to interact with {@link AuthService}.
 * Contains the possibilities to authenticate and register a new user
 */

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/auth", produces = { MediaType.APPLICATION_JSON_VALUE })
public class AuthController {

  private final AuthService authService;

  /**
   * Register a new user.
   * @param userRegisterRequestDto the user data needed to register as {@link UserRegisterRequestDto}
   * @return {@link ResponseEntity} with status <code> ok (200)</code>
   */
  @PostMapping("/register")
  public ResponseEntity<?> registerUser(@Valid @RequestBody
  UserRegisterRequestDto userRegisterRequestDto){
    return ResponseEntity.ok(authService.registerUser(userRegisterRequestDto));
  }

  /**
   * Authenticate a user
   * @param userLoginRequestDto username and password as {@link UserLoginRequestDto}
   * @param response needed to be passed through a cookie for the jwt
   * @return {@link ResponseEntity} with status <code>200</code>
   */
  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody
  UserLoginRequestDto userLoginRequestDto, HttpServletResponse response){
    UserLoginResponseDto userLoginResponseDto = authService.signIn(userLoginRequestDto, response);
    return ResponseEntity.ok(userLoginResponseDto);
  }

}
