package com.example.ozgradacbackend.controller;

import com.example.ozgradacbackend.entity.Post;
import com.example.ozgradacbackend.entity.dto.PostDto;
import com.example.ozgradacbackend.service.PostService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/posts", produces = {MediaType.APPLICATION_JSON_VALUE})
@AllArgsConstructor
public class PostController {

  private final PostService postService;
  private final ModelMapper modelMapper;
  @GetMapping("/")
  public ResponseEntity<List<PostDto>> getAllPosts() {
    List<Post> posts = postService.getAllPosts();
    List<PostDto> postDtos = posts.stream().map(post -> modelMapper.map(post, PostDto.class))
        .collect(Collectors.toList());
    return ResponseEntity.ok(postDtos);
  }

  @PostMapping("/")
  public ResponseEntity<Post> createPost(PostDto postDto){
    return ResponseEntity.ok(postService.createPost(postDto));
  }
}
