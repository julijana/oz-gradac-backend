package com.example.ozgradacbackend.controller;

import com.example.ozgradacbackend.entity.User;
import com.example.ozgradacbackend.entity.dto.UserDto;
import com.example.ozgradacbackend.service.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/user", produces = { MediaType.APPLICATION_JSON_VALUE })
public class UserController {

  private final UserService userService;

  private final ModelMapper modelMapper;

  /**
   * Returns a current logged in user
   * {@link ResponseEntity} with status <code>200</code>
   */
  @GetMapping("/")
  public ResponseEntity<?> getCurrentUser(){

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.getCurrentUser(authentication);

    return ResponseEntity.ok(modelMapper.map(user, UserDto.class));

  }
}
