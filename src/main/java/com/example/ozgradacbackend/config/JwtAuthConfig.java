package com.example.ozgradacbackend.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

@Configuration
@Data
public class JwtAuthConfig {

  private SecretKey secret = generateJwtSecret();

  @Value("${authentication.jwt.expiration}")
  private Long expiration;
  private SecretKey generateJwtSecret(){
    try {
      KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
      keyGenerator.init(256);
      SecretKey secretKey = keyGenerator.generateKey();
      return secretKey;
    } catch (NoSuchAlgorithmException e) {
      throw new SecurityException("Secret for JWT generation could not be created due to {}", e);
    }
  }

}
