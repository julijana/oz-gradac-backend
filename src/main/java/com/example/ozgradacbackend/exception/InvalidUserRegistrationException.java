package com.example.ozgradacbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUserRegistrationException extends RuntimeException{

  public InvalidUserRegistrationException(String message){
    super(message);
  }
}
