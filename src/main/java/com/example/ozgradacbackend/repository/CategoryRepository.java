package com.example.ozgradacbackend.repository;

import com.example.ozgradacbackend.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

  Category getReferenceById(Long id);
}
