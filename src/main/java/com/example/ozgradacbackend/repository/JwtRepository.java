package com.example.ozgradacbackend.repository;

import com.example.ozgradacbackend.entity.Jwt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JwtRepository extends JpaRepository<Jwt, Long> {
  Optional<Jwt> findByToken(String token);
}
